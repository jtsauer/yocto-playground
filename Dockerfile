FROM ubuntu:14.04
LABEL maintainer "johannes.sauer@tomtom.com"

#TODO: find soltution replacing hard-coded uid and gid
RUN groupadd -r yocto -g 10117 && useradd -r -g yocto yocto -d /yocto-build -u 189018
RUN echo "/usr/bin/sudo -u yocto -i /bin/zsh" > /changeUser.sh
RUN echo "yocto ALL=(ALL) ALL" >> /etc/sudoers
RUN echo "yocto:0" | chpasswd

RUN echo "deb http://packages.ros.org/ros/ubuntu trusty main" >> /etc/apt/sources.list.d/official-ros.list

RUN apt-get update

# TODO: check whether the next 4 commands are superfluous
# install transport driver for autonomos repo 
RUN apt-get install -y apt-transport-https

RUN echo "deb [arch=amd64] https://apt.autonomos.xyz/autonomos/general trusty main" >> /etc/apt/sources.list.d/autonomos-general.list

# add key for autonomos repo
COPY trusted.gpg /etc/apt/

# update again (new repo added)
RUN apt-get update

# install yocto dependencies
RUN apt-get install -y --force-yes gawk wget git-core diffstat unzip texinfo gcc-multilib build-essential chrpath socat cpio python python3 python3-pip python-pexpect libsdl1.2-dev xterm openssh-client git

# Install other useful and/or necessary packages
RUN apt-get install -y --force-yes lsb-release tmux zsh u-boot-tools vim

VOLUME /yocto-build
WORKDIR /yocto-build

ENTRYPOINT [ "/bin/zsh", "-e" ,"/changeUser.sh" ]
