#!/bin/bash

docker run --rm \
		--net=host \
		--user=yocto:yocto \
		-v ${PWD}:/yocto-build \
        -ti ubuntu-14.04:yocto-build \
		/bin/zsh
